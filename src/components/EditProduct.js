import React, { useState, useRef } from 'react';
import Error from './shared/Error';

import Axios from 'axios';
import Swal from 'sweetalert2';
// use withRouter in highorder components
import { withRouter } from 'react-router-dom'; 

function EditProduct( props ) {
    // destructuring
    const { history, product, setReloadProducts } = props;

    // generate refs
    const priceProductRef = useRef('');
    const nameProductRef = useRef('');

    const [category, setCategory] = useState('');
    const [error, setError] = useState(false);

    const editProduct = (e) => {
        e.preventDefault();
        const priceProduct = priceProductRef.current.value;
        const nameProduct = nameProductRef.current.value;
        let categoryProduct = (category === '') ? product.category : category;

        if(nameProduct===''|| priceProduct===''){
            setError(true);
            return;
        }
        
        setError(false);
        
        const editProduct = {
            productName: nameProduct,
            productPrice: priceProduct,
            category: categoryProduct
        }
        // call to endpoint to save
        modifyProduct(editProduct, product.id);
        setTimeout(() => {
            // redirect to products
            setReloadProducts(true);
            history.push('/products');
        }, 500);
    }

    const modifyProduct = async (product, id) => {
        //console.log(product);
        try {
            const response = await Axios.put(`http://localhost:4000/restaurant/${ id }`, product);
            if(response.status === 200){
                Swal.fire(
                    'Product Edited',
                    'The product has been edited successfuly',
                    'success'
                )
            }

        } catch (error) {
            console.log(error);
            Swal.fire({
                type: 'error',
                title: 'Product not created',
                text: 'Product has not been created, It was an error',
            })
        }
    }

    const getRadioButton = (e) => {
        setCategory(e.target.value);
    }

    return (
        <div className="col-md-8 mx-auto ">
            <h1 className="text-center">Edit Product</h1>

            { (error) ? <Error message='Please all fields are required...' /> : null }
            <form
                className="mt-5"
                onSubmit={ editProduct }
            >
                <div className="form-group">
                    <label>Product Name</label>
                    <input 
                        type="text" 
                        className="form-control" 
                        name="name" 
                        placeholder="Product Name"
                        ref={ nameProductRef }
                        defaultValue={ product.productName }
                    />
                </div>

                <div className="form-group">
                    <label>Product Price</label>
                    <input 
                        type="number" 
                        className="form-control" 
                        name="price"
                        placeholder="Product Price"
                        ref={ priceProductRef }
                        defaultValue={ product.productPrice }
                    />
                </div>

                <legend className="text-center">Category:</legend>
                <div className="text-center">
                <div className="form-check form-check-inline">
                    <input 
                        className="form-check-input" 
                        type="radio" 
                        name="category"
                        value="dessert"
                        onChange={ getRadioButton }
                        defaultChecked={ (product.category === 'dessert') }
                    />
                    <label className="form-check-label">
                        Dessert
                    </label>
                </div>
                <div className="form-check form-check-inline">
                    <input 
                        className="form-check-input" 
                        type="radio" 
                        name="category"
                        value="drinks"
                        onChange={ getRadioButton }
                        defaultChecked={ (product.category === 'drinks') }
                    />
                    <label className="form-check-label">
                        Drinks
                    </label>
                </div>

                <div className="form-check form-check-inline">
                    <input 
                        className="form-check-input" 
                        type="radio" 
                        name="category"
                        value="cuts"
                        onChange={ getRadioButton }
                        defaultChecked={ (product.category === 'cuts') }
                    />
                    <label className="form-check-label">
                        Cuts
                    </label>
                </div>

                <div className="form-check form-check-inline">
                    <input 
                        className="form-check-input" 
                        type="radio" 
                        name="category"
                        value="salad"
                        onChange={ getRadioButton }
                        defaultChecked={ (product.category === 'salad') }
                    />
                    <label className="form-check-label">
                        Salad
                    </label>
                </div>
                </div>

                <input type="submit" className="font-weight-bold text-uppercase mt-5 btn btn-primary btn-block py-3" value="Edit Product" />
            </form>
        </div>
    )
}

export default withRouter(EditProduct);