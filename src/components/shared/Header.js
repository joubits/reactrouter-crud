import React from 'react';
import { Link, NavLink } from 'react-router-dom'

const Header = () => (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <div className="container">
            <Link to="/" className="navbar-brand" >
                React CRUD & Route
            </Link>

            <ul className="navbar-nav mr-auto">
                <li className="nav-item">
                    <NavLink to="/products" 
                            className="nav-link"
                            activeClassName="active">
                        Products
                    </NavLink>
                </li>
                <li>
                    <NavLink to="/new-product" 
                            className="nav-link"
                            activeClassName="active">
                        Add Product
                    </NavLink>
                </li>

            </ul>
        </div>
    </nav>
);

export default Header
