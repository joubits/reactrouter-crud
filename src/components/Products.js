import React, { Fragment } from 'react';
import ProductList from './ProductList';

function Products( { products, setReloadProducts } ) {
    return (
        <Fragment>
            <h1 className="text-center">List of Products</h1>
            { 
                products.map( (product) => (
                    <ProductList 
                        key={ product.id }
                        product={ product }
                        setReloadProducts={setReloadProducts}
                    />
                ) )
            }
        </Fragment> 
    )
}

export default Products
