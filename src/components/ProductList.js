import React from 'react';
import { Link } from 'react-router-dom';

import Axios from 'axios';
import Swal from 'sweetalert2';

function ProductList({ product, setReloadProducts }) {

    const removeProduct = (id) => {
        Swal.fire({
            title: 'Are you sure to delete Product?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'Cancel'
        }).then( async (result) => {
            if (result.value) {
                try {
                    const response = await Axios.delete(`http://localhost:4000/restaurant/${ id }`);
                    //console.log(response);
                    if(response.status === 200) {
                        Swal.fire(
                            'Deleted!',
                            'Your file has been deleted.',
                            'success'
                        )       
                        // Load products with API again
                        setReloadProducts(true);
                    }
                } catch (error) {
                    Swal.fire({
                        type: 'error',
                        title: 'Error',
                        text: 'It was an error, Try again...'
                    })
                }
            }
          })
    }

    return (
        <li data-categoria={ product.category } 
            className="list-group-item d-flex justify-content-between 
            align-items-center" >
            <p>
                { product.productName } { '  ' }
                <span className="font-weight-bold">${ product.productPrice }</span>
            </p>

            <div>
                <Link to={ `/products/edit/${ product.id }` }
                    className="btn btn-success mr-2">
                    Edit
                </Link>
                <button 
                    type="button"
                    className="btn btn-danger"
                    onClick={ () => removeProduct(product.id) }>
                    
                    Delete
                </button>
            </div>
        </li>
    )
}

export default ProductList
