import React, { useState } from 'react';
import Error from '../shared/Error';

import Axios from 'axios';
import Swal from 'sweetalert2';
// use withRouter in highorder components
import { withRouter } from 'react-router-dom'; 

function AddProduct( { history, setReloadProducts } ) {
    const [productName, setProductName] = useState('');
    const [productPrice, setProductPrice] = useState('');
    const [category, setCategory] = useState('');
    const [error, setError] = useState(false);

    const getRadioButton = (e) => {
        setCategory(e.target.value);
    }

    const addNewProduct = async (productName, productPrice, category) => {
        try {
            const response = await Axios.post('http://localhost:4000/restaurant', {
                productName: productName,
                productPrice: productPrice,
                category: category
            });
            if(response.status === 201){
                Swal.fire(
                    'Product Created',
                    'The product has been created successfuly',
                    'success'
                )
            }
            return;

        } catch (error) {
            console.log(error);
            Swal.fire({
                type: 'error',
                title: 'Product not created',
                text: 'Product has not been created, It was an error',
            })
        }
    }

    // Save product
    const saveProduct = (e) => {
        e.preventDefault();
        if( productName==='' || productPrice==='' || category==='') {
            setError(true);
            return;
        }
        setError(false);

        // create the new product
        addNewProduct(productName, productPrice, category);
        
        setTimeout(() => {
            // redirect to products
            setReloadProducts(true);
            history.push('/products');
        }, 500);
        
    }

    return (
        <div className="col-md-8 mx-auto ">
            <h1 className="text-center">Add New Product</h1>

            { (error) ? <Error message='Please all fields are required...' /> : null }
            <form
                className="mt-5"
                onSubmit={ saveProduct }
            >
                <div className="form-group">
                    <label>Product Name</label>
                    <input 
                        type="text" 
                        className="form-control" 
                        name="name" 
                        placeholder="Product Name"
                        onChange={ e => setProductName(e.target.value) }
                    />
                </div>

                <div className="form-group">
                    <label>Product Price</label>
                    <input 
                        type="number" 
                        className="form-control" 
                        name="price"
                        placeholder="Product Price"
                        onChange={ e => setProductPrice(e.target.value) }
                    />
                </div>

                <legend className="text-center">Category:</legend>
                <div className="text-center">
                <div className="form-check form-check-inline">
                    <input 
                        className="form-check-input" 
                        type="radio" 
                        name="category"
                        value="dessert"
                        onChange={ getRadioButton }
                    />
                    <label className="form-check-label">
                        Dessert
                    </label>
                </div>
                <div className="form-check form-check-inline">
                    <input 
                        className="form-check-input" 
                        type="radio" 
                        name="category"
                        value="drinks"
                        onChange={ getRadioButton }
                    />
                    <label className="form-check-label">
                        Drinks
                    </label>
                </div>

                <div className="form-check form-check-inline">
                    <input 
                        className="form-check-input" 
                        type="radio" 
                        name="category"
                        value="cuts"
                        onChange={ getRadioButton }
                    />
                    <label className="form-check-label">
                        Cuts
                    </label>
                </div>

                <div className="form-check form-check-inline">
                    <input 
                        className="form-check-input" 
                        type="radio" 
                        name="category"
                        value="salad"
                        onChange={ getRadioButton }
                    />
                    <label className="form-check-label">
                        Salad
                    </label>
                </div>
                </div>

                <input type="submit" className="font-weight-bold text-uppercase mt-5 btn btn-primary btn-block py-3" value="Add Product" />
            </form>
        </div>
    )
}

export default withRouter(AddProduct);