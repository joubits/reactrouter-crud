import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import axios from 'axios';

import Header from './components/shared/Header';
import Products from './components/Products';
import AddProduct from './components/Product/AddProduct';
import EditProduct from './components/EditProduct';
import SingleProduct from './components/SingleProduct';

function App() {
  const [ products, setProducts ] = useState([]);
  const [ reloadProducts, setReloadProducts ] = useState(true);

  useEffect( () => {
    if(reloadProducts){
      console.log('Reload products...');
      getProducts();
      setReloadProducts(false);
    }
    
  }, [reloadProducts] )

  const getProducts = async () => {
    const url = 'http://localhost:4000/restaurant';
    const resultado = await axios.get(url);
    setProducts(resultado.data);
  }

  return (
    <Router>
      {/* Include header in all routes */}
      <Header />
      <main className="container mt-5">
        <Switch>
        <Route exact path="/" 
              render={ () => (
                <Products 
                  products={ products }
                  setReloadProducts={setReloadProducts}
                  />
                ) }  
          />
          <Route exact path="/products" 
              render={ () => (
                <Products 
                  products={ products }
                  setReloadProducts={setReloadProducts}
                  />
                ) }  
          />
          <Route exact path="/new-product" 
              render = { () => (
                <AddProduct setReloadProducts={setReloadProducts} />
              )

            }
          />
          <Route exact path="/products/:id" component={ SingleProduct }  />
          <Route exact path="/products/edit/:id" 
              render = { props => {
                const productId = parseInt(props.match.params.id);
                // filter the product with productId
                const product = products.filter( product => product.id === productId );
                //console.log(product[0]);

                return (
                  <EditProduct 
                      product={ product[0] }
                      setReloadProducts={ setReloadProducts }  
                  />
                );
              } }
          />

        </Switch>
        <p className="mt-4 p2 text-center">Todos los derechos reservados - Joubits</p>
      </main>
    </Router>
  );
}

export default App;
